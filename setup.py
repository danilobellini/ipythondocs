# -*- coding: utf-8 -*-
#
# Copyright (c) 2013
# Danilo de Jesus da Silva Bellini and Wille Marcel Lima Malheiro
#
# This is free software (MIT license). See LICENSE.txt for more details.
# Created on Sat Oct 05 2013

from setuptools import setup

with open("README.rst") as f:
    long_description = f.read()

if __name__ == "__main__":
    setup(
        name="pytest-ipythondocs",
        description="pytest plugin to check IPython tests inside docstrings",
        long_description=long_description,
        version="0.0.1",
        author="Danilo de Jesus da Silva Bellini and " \
               "Wille Marcel Lima Malheiro",
        author_email="danilo.bellini@gmail.com ; wille@wille.blog.br",
        url="",
        py_modules=["pytest_ipythondocs"],
        entry_points={"pytest11": ["ipythondocs = pytest_ipythondocs"]},
    )

